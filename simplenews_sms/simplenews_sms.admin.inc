<?php

/**
 * Callback for SMS admin page
 */
function simplenews_sms_manage() {
  $render_array['form'] = drupal_get_form('simplenews_sms_admin_settings_form');
  return $render_array;
}

/**
 * SMS Settings page form
 */
function simplenews_sms_admin_settings_form() {
  $form['twilio'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twilio Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['twilio']['simplenews_sms_twilio_sid']  = array(
    '#type' => 'textfield',
    '#title' => t('Account SID'),
    '#default_value' => variable_get('simplenews_sms_twilio_sid',''),
    '#description' => t('The Twilio Account SID.'),
  );

  $form['twilio']['simplenews_sms_twilio_auth_token']  = array(
    '#type' => 'textfield',
    '#title' => t('Twilio Auth Token'),
    '#default_value' => variable_get('simplenews_sms_twilio_auth_token',''),
    '#description' => t('The Twilio Auth Token.'),
  );
  
  $form['twilio']['simplenews_sms_twilio_from_phone']  = array(
    '#type' => 'textfield',
    '#title' => t('Twilio From Phone'),
    '#default_value' => variable_get('simplenews_sms_twilio_from_phone',''),
    '#description' => t('The phone number to send messages from.'),
  );
  
  $form['twilio']['simplenews_sms_confirm_message']  = array(
    '#type' => 'textfield',
    '#title' => t('Confirm Message'),
    '#default_value' => variable_get('simplenews_sms_confirm_message','We would love to send you an occasional special offer and announcement. Reply START to begin receiving messages'),
    '#description' => t('The message to send asking for confirmation.'),
  );
  
  $form['twilio']['simplenews_sms_error_message']  = array(
    '#type' => 'textfield',
    '#title' => t('Confirm Message'),
    '#default_value' => variable_get('simplenews_sms_error_message', "We don't know why, but we couldn't start your subscrition. Call us for help."),
    '#description' => t('The message to send asking for confirmation.'),
  );
  
  $form['twilio']['simplenews_sms_confirmation_message']  = array(
    '#type' => 'textfield',
    '#title' => t('Confirmation Message'),
    '#default_value' => variable_get('simplenews_sms_confirmation_message', 'You will now receive an occasional special and announcement from us. Reply STOP to stop receiving messages.'),
    '#description' => t('The message to send after confirmation is received.'),
  );
  
  $form['twilio']['simplenews_sms_cancel_message']  = array(
    '#type' => 'textfield',
    '#title' => t('Confirmation Message'),
    '#default_value' => variable_get('simplenews_sms_cancel_message', 'You will no longer receive text messages. Reply START to start receiving them again.'),
    '#description' => t('The message to send after confirmation is received.'),
  );
  
  $form['twilio']['simplenews_sms_unknown_message']  = array(
    '#type' => 'textfield',
    '#title' => t('Confirmation Message'),
    '#default_value' => variable_get('simplenews_sms_unknown_message', 'Unknown Command - Call us for help'),
    '#description' => t('The message to send if an unknown command is received.'),
  );
  
  $form['twilio']['simplenews_sms_default_test_number']  = array(
    '#type' => 'textfield',
    '#title' => t('Default Test Phone'),
    '#default_value' => variable_get('simplenews_sms_default_test_number', ''),
    '#description' => t('Default cell number to send test SMS messages to. No Spaces, No Dashes'),
  );
  
  $form = system_settings_form($form);
  return $form;
}

/**
 * List all subscriptions
 */
function simplenews_sms_admin_subscription() {
  $form['admin'] = simplenews_sms_subscription_list_form();
  return $form;
}

/**
 * Show all SMS Phone numbers subscribed
 */
function simplenews_sms_subscription_list_form() {


  // Table header. Used as tablesort default
  $header = array(
    'phone' => array('data' => t('Phone'), 'field' => 's.phone', 'sort' => 'asc'),
    'source' => array('data' => t('Source'), 'field' => 's.source'),
    'created' => array('data' => t('Created'), 'field' => 's.created'),
    'confirmed' => array('data' => t('Confirmed'), 'field' => 's.confirmed'),
    'canceled' => array('data' => t('Canceled'), 'field' => 's.canceled'),
  );

  $query = db_select('simplenews_sms_subscription', 's')->extend('PagerDefault')->extend('TableSort');
  //simplenews_build_subscription_filter_query($query);
  $result = $query
    ->fields('s', array('sid','phone', 'source', 'created', 'confirmed', 'canceled'))
    ->limit(30)
    ->orderByHeader($header)
    ->execute();

  $options = array();
  $destination = drupal_get_destination();

  foreach ($result as $subscriber) {
    $options[$subscriber->sid] = array(
      'phone' => $subscriber->phone,
      'source' => $subscriber->source,
      'created' => $subscriber->created ? format_date($subscriber->created) : t('Never'),
      'confirmed' => $subscriber->confirmed ? format_date($subscriber->confirmed) : t('Never'),
      'canceled' => $subscriber->canceled ? format_date($subscriber->canceled) : t('Never'),
    );
  }

  $form['subscribers'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No subscribers available.'),
  );

  $form['pager'] = array(
    // Calling theme('pager') directly so that it the first call after the
    // pager query executed above.
    '#markup' => theme('pager'),
  );
  return $form;
}

/**
 * Mass add phone numbers
 */
function simplenews_sms_subscription_list_add($form, &$form_state) {
  global $language;
  $form['numbers'] = array(
    '#type' => 'textarea',
    '#title' => t('Phone Numbers'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Phone Numbers must be separated by comma, space or newline.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  return $form;
}

/**
 * Callback for Subscription adds
 */
function simplenews_sms_subscription_list_add_submit($form, &$form_state) {
  $added = array();
  $invalid = array();
  
  $numbers = preg_split("/[\s,]+/", $form_state['values']['numbers']);
  foreach ($numbers as $number) {
    //Remove non numeric chars
    $number = preg_replace('/[^0-9]/Uis', '', $number);    
    $number = trim($number);
    if (!$number) {
      continue;
    }
    if (strlen($number) == 10) {
        simplenews_sms_subscribe($number, 'Web Import');
        $added[] = $email;
    } else {
      $invalid[] = $email;
    }
  }
  if ($added) {
    $added = implode(", ", $added);
    drupal_set_message(t('The following numbers were added: %added.', array('%added' => $added)));
  } else {
    drupal_set_message(t('No numbers were added.'));
  }
  if ($invalid) {
    $invalid = implode(", ", $invalid);
    drupal_set_message(t('The following numbers were invalid: %invalid.', array('%invalid' => $invalid)), 'error');
  }
  // Return to the parent page.
  $form_state['redirect'] = 'admin/people/simplenews_sms';
}