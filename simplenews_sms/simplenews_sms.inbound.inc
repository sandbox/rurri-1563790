<?php

function _simplenews_sms_startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

/**
 * Callback for an inbound SMS Message from Twilio
 */
function simplenews_sms_received() {
 
  $vars = array('%Body' => $_REQUEST['Body'],
    '%From' => $_REQUEST['From'],
    '%To' => $_REQUEST['To'],
    '%FromCity' => $_REQUEST['FromCity'],
    '%FromState' => $_REQUEST['FromState'],
    '%FromZip' => $_REQUEST['FromZip'],
    '%SmsSid' => $_REQUEST['SmsSid'],
    '%AccountSid' => $_REQUEST['AccountSid'],
    '%FromCountry' => $_REQUEST['FromCountry'],
    '%SmsStatus' => $_REQUEST['SmsStatus'],
    '%ApiVersion' => $_REQUEST['ApiVersion'],    
  );

  $phone = $_REQUEST['From'];
  $phone = str_replace('+1', '', $phone);
  
  watchdog('simplenews_sms', 'Received SMS Message From %From with Body: %Body', $vars, WATCHDOG_INFO);

  if (trim($_REQUEST['Body'])) {
    $keywords = explode(' ', trim($_REQUEST['Body']), 2);
    $keyword = strtolower($keywords[0]);
    switch ($keyword) {
      case 'start' :
      case 'begin' :
        simplenews_sms_confirm($phone);
        break;
      case 'stop':
      case 'quit':
      case 'cancel':
      case 'unsubscribe':
      case 'end':
        simplenews_sms_cancel($phone);
        break;
      default:
        $subscription = simplenews_sms_get_subscription($_REQUEST['From']);
        if (!$subscription || $subscription->canceled) {
          simplenews_sms_subscribe($phone);
        } else if (!$subscription->confirmed) {
          simplenews_sms_nudge_confirm($phone);
        } else {
          simplenews_sms_send_sms($_REQUEST['From'], variable_get('simplenews_sms_unknown_message', 'Unknown Command - Call us for help'));
        }
        break;
    } // end switch
  } //end if Body
  
  drupal_add_http_header('content-type', 'text/xml; utf-8');
  echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  echo "<Response />";
}